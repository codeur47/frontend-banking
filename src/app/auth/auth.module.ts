import { NgModule } from '@angular/core';
import {SignupComponent} from './signup/signup.component';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './login/login.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [SignupComponent, LoginComponent],
  imports: [CommonModule, ReactiveFormsModule, RouterModule],
  exports: [SignupComponent, LoginComponent]
})
export class AuthModule { }
