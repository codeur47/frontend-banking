import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { LoginRequestPayload } from './login-request.payload';
import { AuthService } from '../shared/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginRequestPayload: LoginRequestPayload;

  constructor(private authService: AuthService, private activatedRoute: ActivatedRoute,
              private router: Router, private toastR: ToastrService, fb: FormBuilder) {
    this.loginRequestPayload = {
      username: '',
      password: ''
    };

    this.loginForm = fb.group(this.loginRequestPayload);
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });

    this.activatedRoute.queryParams
      .subscribe(params => {
        if (params.registered !== undefined && params.registered === 'true') {
          this.toastR.success('Signup Successful. Please Check your inbox for activation email activate your account before you Login! 😀');
        }
      });
  }

  login(): void {
    this.loginRequestPayload.username = this.loginForm.get('username')?.value;
    this.loginRequestPayload.password = this.loginForm.get('password')?.value;

    this.authService.login(this.loginRequestPayload).subscribe(data => {
      this.router.navigateByUrl('/dashboard').then(r =>
        this.toastR.success('Login Successful 😀')
      );
    }, error => {
      throwError(error);
      this.toastR.error('Login Failed. Please check your credentials and try again.  😟');
    });
  }

}
