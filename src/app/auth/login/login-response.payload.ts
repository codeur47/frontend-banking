import {UserResponsePayload} from '../../model/user.response.payload';

export interface LoginResponse {
    authenticationToken: string;
    refreshToken: string;
    expiresAt: Date;
    userResponse: UserResponsePayload;
}
