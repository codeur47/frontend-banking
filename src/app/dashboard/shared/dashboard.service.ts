import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {TransactionRequestPayload} from '../../model/transaction.request.payload';
import {TransactionResponsePayload} from '../../model/transaction.response.payload';
import {TransactionBetweenAccountRequestPayload} from '../../model/transaction.between.account.request.payload';
import {RecipientRequestPayload} from '../../model/recipient.request.payload';
import {RecipientResponsePayload} from '../../model/recipient.response.payload';
import {AppointmentRequestPayload} from '../../model/appointment.request.payload';
import {AppointmentResponsePayload} from '../../model/appointment.response.payload';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private httpClient: HttpClient) {}

  saveTransaction(transactionRequestPayload: TransactionRequestPayload): Observable<any> {
    return this.httpClient.post('http://localhost:8080/api/transaction', transactionRequestPayload, { responseType: 'text' });
  }

  saveTransactionToOther(transactionBetweenAccountRequestPayload: TransactionBetweenAccountRequestPayload): Observable<any> {
    return this.httpClient.post('http://localhost:8080/api/transaction/other', transactionBetweenAccountRequestPayload, { responseType: 'text' });
  }

  saveRecipient(recipientRequestPayload: RecipientRequestPayload): Observable<any> {
    return this.httpClient.post('http://localhost:8080/api/recipient', recipientRequestPayload, { responseType: 'text' });
  }

  saveAppointment(appointmentRequestPayload: AppointmentRequestPayload): Observable<any>{
    return this.httpClient.post('http://localhost:8080/api/appointment', appointmentRequestPayload, { responseType: 'text' });
  }

  getAllAppointment(): Observable<AppointmentResponsePayload[]> {
    return this.httpClient.get<AppointmentResponsePayload[]>('http://localhost:8080/api/appointment');
  }

  getAllPrimaryTransaction(): Observable<TransactionResponsePayload[]> {
    return this.httpClient.get<TransactionResponsePayload[]>('http://localhost:8080/api/transaction/primary');
  }

  getAllSavingsTransaction(): Observable<TransactionResponsePayload[]> {
    return this.httpClient.get<TransactionResponsePayload[]>('http://localhost:8080/api/transaction/savings');
  }

  getAllRecipients(): Observable<RecipientResponsePayload[]> {
    return this.httpClient.get<RecipientResponsePayload[]>('http://localhost:8080/api/recipient');
  }

  getRecipientById(recipientId: number): Observable<RecipientResponsePayload> {
    return this.httpClient.get<RecipientResponsePayload>('http://localhost:8080/api/recipient/' + recipientId);
  }

  deleteRecipient(recipientId: number): Observable<any> {
    return this.httpClient.delete('http://localhost:8080/api/recipient/' + recipientId);
  }
}
