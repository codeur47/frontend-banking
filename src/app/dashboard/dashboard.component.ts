import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {TransactionDialogComponent} from './dialog/transaction-dialog.component';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TransactionRequestPayload} from '../model/transaction.request.payload';
import {DashboardService} from './shared/dashboard.service';
import {ToastrService} from 'ngx-toastr';
import {TransactionResponsePayload} from '../model/transaction.response.payload';
import {UserResponsePayload} from '../model/user.response.payload';
import {AuthService} from '../auth/shared/auth.service';
import {TransactionType} from '../model/transaction.type';
import {TransactionBetweenAccountRequestPayload} from '../model/transaction.between.account.request.payload';
import {DialogType} from '../model/dialog.type';
import {RecipientRequestPayload} from '../model/recipient.request.payload';
import {RecipientResponsePayload} from '../model/recipient.response.payload';
import {AppointmentResponsePayload} from '../model/appointment.response.payload';
import {AppointmentRequestPayload} from '../model/appointment.request.payload';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit{

  Deposit = TransactionType.DEPOSIT;
  BetweenAccount = TransactionType.BETWEEN_ACCOUNT;
  NotBetweenAccount = TransactionType.NOT_BETWEEN_ACCOUNT;
  Recipient = DialogType.RECIPIENT;
  RecipientDelete = DialogType.DELETE_RECIPIENT;
  Appointment = DialogType.APPOINTMENT;
  recipientId = 0;
  transactionType = '';
  transactionForm: FormGroup;
  transactionFormBetween: FormGroup;
  recipientForm: FormGroup;
  appointmentForm: FormGroup;
  transactionRequestPayload: TransactionRequestPayload;
  transactionBetweenAccountRequestPayload: TransactionBetweenAccountRequestPayload;
  recipientRequestPayload: RecipientRequestPayload;
  appointmentRequestPayload: AppointmentRequestPayload;
  primaryTransactionResponsePayload: TransactionResponsePayload[] = [];
  savingsTransactionResponsePayload: TransactionResponsePayload[] = [];
  recipientResponsePayload: RecipientResponsePayload[] = [];
  appointmentResponsePayload: AppointmentResponsePayload[] = [];
  userResponse: UserResponsePayload = {
    userId: 0,
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    enabled: false,
    primaryAccountResponse:  {
      id: 0,
      accountNumber: '',
      accountBalance: 0
    },
    savingsAccountResponse: {
      id: 0,
      accountNumber: '',
      accountBalance: 0
    }
  };
  isLoggedIn?: boolean;

  constructor(private dialog: MatDialog, fb: FormBuilder,
              private dashboardService: DashboardService,
              private toastR: ToastrService,
              private authService: AuthService){
    this.transactionForm = fb.group(this.transactionRequestPayload = {
      transactionType: '',
      accountType: '',
      description: '',
      amount: 0
    });
    this.transactionFormBetween = fb.group(this.transactionBetweenAccountRequestPayload = {
      accountType: '',
      accountNumberToTransfer: '',
      description: '',
      amount: 0
    });
    this.recipientForm = fb.group(this.recipientRequestPayload = {
      recipientId: 0,
      name: '',
      email: '',
      phone: '',
      accountNumber: '',
      description: ''
    });
    this.appointmentForm = fb.group(this.appointmentRequestPayload = {
      appointmentId: 0,
      date:  '',
      location: '',
      description: '',
      status: ''
    });
  }

  ngOnInit(): void {
    this.initTransactionForm();

    this.getAllPrimaryTransaction();
    this.getAllSavingsTransaction();
    this.getAllRecipients();
    this.getAllAppointment();
    this.getUserInfoForUserInfoComponent();

  }

  private getUserInfoForUserInfoComponent() {
    if (this.authService.isLoggedIn()) {
      this.userResponse = this.authService.getUserInfo();
      if (this.primaryTransactionResponsePayload.length > 0) {
        this.userResponse.primaryAccountResponse.accountBalance = this.primaryTransactionResponsePayload[this.primaryTransactionResponsePayload.length - 1].availableBalance;
      }
      if (this.savingsTransactionResponsePayload.length > 0) {
        this.userResponse.savingsAccountResponse.accountBalance = this.savingsTransactionResponsePayload[this.savingsTransactionResponsePayload.length - 1].availableBalance;
      }
      this.authService.saveUserInfo(this.userResponse);
    }
  }

  private initTransactionForm(): void {
    this.transactionForm = new FormGroup({
      transactionType: new FormControl('', Validators.required),
      accountType: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required)
    });
    this.transactionFormBetween = new FormGroup({
      accountType: new FormControl('', Validators.required),
      accountNumberToTransfer: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required)
    });
    this.recipientForm = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      accountNumber: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    });
    this.appointmentForm = new FormGroup({
      date: new FormControl('', Validators.required),
      location: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required)
    });
  }

  openDialog(transactionType: string, recipientId?: number): void {
    this.transactionType = transactionType;
    const dialogRef = this.dialog.open(TransactionDialogComponent, {
      data: {
        transactionForm: this.transactionForm,
        transactionFormBetween: this.transactionFormBetween,
        recipientForm: this.recipientForm,
        appointmentForm: this.appointmentForm,
        transactionType: this.transactionType
      },
      minWidth: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result){
        if (this.transactionType === this.NotBetweenAccount){
          this.saveTransaction();
        }
        if (this.transactionType === this.BetweenAccount){
          this.saveTransactionToOther();
        }
        if (this.transactionType === this.Recipient){
          this.saveRecipient();
        }
        if (this.transactionType === this.RecipientDelete){
          if (recipientId !== undefined) {
            this.recipientId = recipientId;
          }
          this.deleteRecipient();
        }
        if (this.transactionType === this.Appointment){
          this.saveAppointment();
        }
      }else{
        console.log('No');
      }
    });
  }

  saveTransaction(): void {
    this.transactionRequestPayload.transactionType = this.transactionForm.get('transactionType')?.value;
    this.transactionRequestPayload.description = this.transactionForm.get('description')?.value;
    this.transactionRequestPayload.accountType = this.transactionForm.get('accountType')?.value;
    this.transactionRequestPayload.amount = this.transactionForm.get('amount')?.value;

    this.dashboardService.saveTransaction(this.transactionRequestPayload).subscribe(result => {
      this.toastR.success('Transaction saved successfully  😃.');
      this.getAllPrimaryTransaction();
      this.getAllSavingsTransaction();
      this.getUserInfoForUserInfoComponent();
    }, error => {
      this.toastR.error('Transaction Failed  😟.');
    });
  }

  saveTransactionToOther(): void {
    this.transactionBetweenAccountRequestPayload.accountType = this.transactionFormBetween.get('accountType')?.value;
    this.transactionBetweenAccountRequestPayload.accountNumberToTransfer = this.transactionFormBetween.get('accountNumberToTransfer')?.value;
    this.transactionBetweenAccountRequestPayload.description = this.transactionFormBetween.get('description')?.value;
    this.transactionBetweenAccountRequestPayload.amount = this.transactionFormBetween.get('amount')?.value;

    this.dashboardService.saveTransactionToOther(this.transactionBetweenAccountRequestPayload).subscribe(result => {
      this.toastR.success('Transaction saved successfully  😃.');
      this.getAllPrimaryTransaction();
      this.getAllSavingsTransaction();
    }, error => {
      this.toastR.error('Transaction Failed  😟.');
    });
  }

  saveRecipient(): void {
    this.recipientRequestPayload.name = this.recipientForm.get('name')?.value;
    this.recipientRequestPayload.email = this.recipientForm.get('email')?.value;
    this.recipientRequestPayload.phone = this.recipientForm.get('phone')?.value;
    this.recipientRequestPayload.accountNumber = this.recipientForm.get('accountNumber')?.value;
    this.recipientRequestPayload.description = this.recipientForm.get('description')?.value;

    this.dashboardService.saveRecipient(this.recipientRequestPayload).subscribe(result => {
      this.toastR.success('Recipient saved successfully  😃.');
      this.getAllRecipients();
    }, error => {
      this.toastR.error('Recipient registration Failed  😟.');
    });
  }

  saveAppointment(): void {
    this.appointmentRequestPayload.date = this.appointmentForm.get('date')?.value;
    this.appointmentRequestPayload.description = this.appointmentForm.get('description')?.value;
    this.appointmentRequestPayload.status = 'NEW';
    this.appointmentRequestPayload.location = this.appointmentForm.get('location')?.value;

    this.dashboardService.saveAppointment(this.appointmentRequestPayload).subscribe(result => {
      this.toastR.success('Appointment saved successfully  😃.');
      this.getAllAppointment();
    }, error => {
      this.toastR.error('Recipient registration Failed  😟.');
    });
  }

  getAllPrimaryTransaction(): void {
    this.dashboardService.getAllPrimaryTransaction().subscribe(result => {
      this.primaryTransactionResponsePayload = result;
      console.log(JSON.stringify('TRANSACTIONS==============>' + result));
    }, error => this.toastR.error('Failed to load primary transaction list.  😟'));
  }

  getAllSavingsTransaction(): void {
    this.dashboardService.getAllSavingsTransaction().subscribe(result => {
      this.savingsTransactionResponsePayload = result;
      console.log(JSON.stringify('SAVINGS==============>' + result));
    }, error => this.toastR.error('Failed to load savings transaction list  😌.'));
  }

  getAllRecipients(): void {
    this.dashboardService.getAllRecipients().subscribe(result => {
      this.recipientResponsePayload = result;
      console.log(JSON.stringify('RECIPIENT==============>' + result));
    }, error => this.toastR.error('Failed to load recipient list  😌.'));
  }

  getAllAppointment(): void {
    this.dashboardService.getAllAppointment().subscribe(result => {
      this.appointmentResponsePayload = result;
      console.log(JSON.stringify('APPOINTMENT==============>' + result));
    }, error => this.toastR.error('Failed to load appointment list  😌.'));
  }

  deleteRecipient(): void {
    this.dashboardService.deleteRecipient(this.recipientId).subscribe(result => {
      this.toastR.success('Recipient deletion successfully  😃');
      this.getAllRecipients();
    }, error => {
      this.toastR.error('Recipient deletion failed  😌.');
      console.log('Error' + error.message);
    });
  }

  getRecipientById(recipientId: number): void {
    const fb: FormBuilder = new FormBuilder();
    this.dashboardService.getRecipientById(recipientId).subscribe(result => {
      this.recipientForm = fb.group(this.recipientRequestPayload = {
        recipientId: result.recipientId,
        name: result.name,
        email: result.email,
        phone: result.phone,
        accountNumber: result.accountNumber,
        description: result.description
      });
      this.openDialog(this.Recipient);
    }, error => {
      this.toastR.error('Failed to get recipient');
      console.log('Error' + error.message);
    });
  }
}


