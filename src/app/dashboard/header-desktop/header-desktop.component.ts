import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../auth/shared/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header-desktop',
  templateUrl: './header-desktop.component.html'
})
export class HeaderDesktopComponent implements OnInit {

  constructor(private toastR: ToastrService,
              private authService: AuthService,
              private router: Router){}
  ngOnInit(): void {}

  logout(): void {
    this.authService.logout().subscribe(result => {
      this.authService.clearUserData();
      this.router.navigateByUrl('').then(r => this.toastR.error('Logout successful  😀.'));
    }, error => this.toastR.error('Failed to logout  😌.'));
    this.router.navigateByUrl('');
  }
}
