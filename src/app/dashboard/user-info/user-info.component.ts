import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {UserResponsePayload} from '../../model/user.response.payload';
import {TransactionResponsePayload} from '../../model/transaction.response.payload';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html'
})
export class UserInfoComponent implements OnInit {

  @Input() userResponse?: UserResponsePayload;

  constructor(){}
  ngOnInit(): void {}
}
