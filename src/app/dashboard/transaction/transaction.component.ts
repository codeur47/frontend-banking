import {Component, Input, OnInit} from '@angular/core';
import {TransactionType} from '../../model/transaction.type';
import {TransactionResponsePayload} from '../../model/transaction.response.payload';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html'
})
export class TransactionComponent implements OnInit {

  @Input() transaction?: TransactionResponsePayload;
  Deposit = TransactionType.DEPOSIT;

  constructor(){}
  ngOnInit(): void {}
}
