import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatDialogModule} from '@angular/material/dialog';
import {TransactionDialogComponent} from './transaction-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [TransactionDialogComponent],
  imports: [CommonModule, MatDialogModule, ReactiveFormsModule],
  exports: [TransactionDialogComponent ]
})
export class TransactionDialogModule {}
