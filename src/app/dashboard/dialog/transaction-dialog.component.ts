import {Component, Inject, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {TransactionType} from '../../model/transaction.type';
import {DialogType} from '../../model/dialog.type';

@Component({
  selector: 'app-transaction-dialog',
  templateUrl: './transaction-dialog.component.html',
  styleUrls: ['./transaction-dialog.component.css']
})

export class TransactionDialogComponent implements OnInit {
  Deposit = TransactionType.DEPOSIT;
  Withdraw = TransactionType.WITHDRAW;
  BetweenAccount = TransactionType.BETWEEN_ACCOUNT;
  NotBetweenAccount = TransactionType.NOT_BETWEEN_ACCOUNT;
  Recipient = DialogType.RECIPIENT;
  RecipientDelete = DialogType.DELETE_RECIPIENT;
  Appointment = DialogType.APPOINTMENT;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any){}
  ngOnInit(): void {}
}
