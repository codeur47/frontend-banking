import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {DashboardComponent} from './dashboard.component';
import {HeaderDesktopComponent} from './header-desktop/header-desktop.component';
import {HeaderMobileComponent} from './header-mobile/header-mobile.component';
import {UserInfoComponent} from './user-info/user-info.component';
import {TransactionComponent} from './transaction/transaction.component';
import {RecipientComponent} from './recipient/recipient.component';


@NgModule({
  declarations: [DashboardComponent, HeaderMobileComponent, HeaderDesktopComponent, UserInfoComponent, TransactionComponent, RecipientComponent ],
  imports: [CommonModule, ReactiveFormsModule, RouterModule],
  exports: [DashboardComponent, HeaderMobileComponent, HeaderDesktopComponent, UserInfoComponent, TransactionComponent, RecipientComponent ]
})
export class DashboardModule { }
