export interface AppointmentRequestPayload{
  appointmentId: number;
  date: string;
  location: string;
  description: string;
  status: string;
}
