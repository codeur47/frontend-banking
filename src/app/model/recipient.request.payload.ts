export interface RecipientRequestPayload{
  recipientId: number;
  name: string;
  email: string;
  phone: string;
  accountNumber: string;
  description: string;
}
