export interface TransactionBetweenAccountRequestPayload {
  accountType: string;
  accountNumberToTransfer: string;
  description: string;
  amount: number;
}
