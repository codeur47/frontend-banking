import {PrimaryAccountResponsePayload} from './primary.account.response.payload';
import {SavingsAccountResponsePayload} from './savings.account.response.payload';

export interface UserResponsePayload {
  userId: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  enabled: boolean;
  primaryAccountResponse: PrimaryAccountResponsePayload;
  savingsAccountResponse: SavingsAccountResponsePayload;
}
