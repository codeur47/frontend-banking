export interface PrimaryAccountResponsePayload {
  id: number;
  accountNumber: string;
  accountBalance: number;
}

