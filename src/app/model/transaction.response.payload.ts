export interface TransactionResponsePayload {
  id: number;
  date: Date;
  description: string;
  type: string;
  status: string;
  amount: number;
  availableBalance: number;
}
