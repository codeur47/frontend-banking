export interface SavingsAccountResponsePayload {
  id: number;
  accountNumber: string;
  accountBalance: number;
}
