export interface TransactionRequestPayload {
  transactionType: string;
  accountType: string;
  description: string;
  amount: number;
}
