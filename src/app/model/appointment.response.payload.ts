export interface AppointmentResponsePayload{
  appointmentId: number;
  date: string;
  location: string;
  description: string;
  status: string;
}
